# Glosarium

Kosa kata dalam Bahasa Indonesia yang sering digunakan

## Panduan

1. Glosarium padanan kata di kateglo.com
2. kbbi.kemdikbud.go.id
3. Cari di twitter ivanlanin dengan cara "from:ivanlanin padanan-kata-yang-dicari" atau bertanya langsung pada yang bersangkutan jika tidak menemukan
4. Bertanya ke komunitas penerjemah Libre Office
5. Langkah terakhir janjian dengan semua penerjemah untuk penggunaan kata/istilah yang belum ada atau tidak umum
6. Google Translate bisa dijadikan sebagai salah satu opsi terjemahan cepat tapi masih perlu disunting.

## Daftar Kata

Daftar kata di bawah ini kita sediakan untuk memudahkan rekan-rekan *volunteer* dalam memahami istilah teknis yang digunakan pada proyek ini (Diurutkan sesuai abjad):

1. **Epics**\
   Merupakan kumpulan dari beberapa *issues* yang saling berkaitan
2. **PIC**\
   Person in Charge
3. ...
