---
prev: /workflow-content/2-approving-content
next: false
---

# Workflow Release (For IT)

Alur kerja rilis konten untuk Administrator Wordpress

## Workflow Graph

```mermaid
sequenceDiagram
  participant Approval Team
  participant Administrator
  participant Front End (Website)
  Approval Team ->> Administrator: Update Koordinator
  note over Approval Team, Administrator: Google Docs File
  Administrator ->> Administrator: Copy & Reformat
  note over Administrator: Wordpress Draft
  Administrator ->> Administrator: Publish
  note over Administrator: Wordpress Post
  Administrator -->> Front End (Website): Netlify Push
  note over Front End (Website): Published Post
  Administrator --> Approval Team: Check formatting
```

## _Standard Operating Procedure_ untuk rilis konten

Semua konten yang berada di [folder Google Docs `for release-IT`](https://kcov.id/for-release)
sudah bisa ditambahkan ke [Wordpress kawalcovid19](https://kcov.id/wp).

PS. Kita juga punya [*tracking sheet*](https://kcov.id/tracker)
tetapi masih belum *up to date*

### Langkah langkah

1. Buka folder [Google Drive `for release - IT`](kcov.id/for-release)
2. Pilih dokumen yang dihimbau untuk dirilis
    * Prioritaskan dokumen yang memiliki prefix `Priority`
    * Dokumen harus sudah diapprove (Ada prefix `APPROVE` atau `APPR`)
3. Periksa dokumen, apakah masih ada komentar yang belum diselesaikan
	Apabila masih ada komentar yang belum tuntas seperti ini:
  ![Unresolved comment example](~@images/unresolved-comment-example.png)
  Angkat ke channel `#team-content` di Slack untuk segera diselesaikan.
  ![Unresolved comment escalation example](~@images/unresolved-comment-escalation-example.png)
4. Salin isi dokumen
5. Buat post baru di Wordpress dan tempelkan `Ctrl+V` (_Paste_) konten
  ![WordPress add new post button](~@images/wordpress-add-new-post-button.png)

6. Rapihkan konten secukupnya, perhatikan semantik artikel
    * **Urutan heading**: H2, H3, Bold Paragraph
    * Heading tidak di bold
    * Gambar diperiksa ulang dan diberi alt text untuk aksesibilitas
  ![Tidy up content semantic](~@images/tidy-up-content-semantic.png)

7. Perbarui metadata artikel
    * Apabila profil pengarang tidak disampaikan, ganti kolom `Author` ke `Kawal COVID-19`.
    * Pilih kategori terkait di kolom `Category`
    * Tambahkan ringkasan ke kolom `Excerpt` untuk pratinjau di media sosial.
      * Ambil kalimat inti yang sekiranya mewakili isi artikel secara keseluruhan.

	Contoh metadata:
  ![Article metadata](~@images/article-metadata.png)

8. Klik tombol `Publish` dan konfirmasi dengan klik `Publish` sekali lagi
   Pastikan tidak ada kesalahan.
	 Apabila ditemukan kesalahan,kembali ke langkah ke-6 di atas.
  ![Publish button](~@images/publish-button.png)
  ![Publish confirmation](~@images/publish-confirmation.png)

9. Ubah prefix nama dokumen di Google Drive menjadi `PUBLISHED`
  ![Rename published docs](~@images/rename-published-docs.png)

10. Klik tombol `Trigger Netlify Deploy`

	Artikel akan dirilis di [kawalcovid19.id](https://kawalcovid19.id)\
	Artikel baru akan muncul setelah beberapa menit, biasanya kurang dari lima menit.\
  Usahakan grouping artikel yang di deploy untuk irit resource.
  ![Publish article preview](~@images/published-article-preview.png)
  ![Publish article content](~@images/published-article-content.png)
