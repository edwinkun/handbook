# Menggunakan Link Shortener kcov.id

Kita memiliki *link shortener* tersendiri untuk memudahkan mengingat dan mengakses pranala-pranala yang penting dan dibutuhkan oleh baik *user* maupun *tim* dalam proses pembangunan. Pranala tersebut dapat diakses dengan awalan `kcov.id/` dan ditambahkan dengan *path* lanjutannya. Beberapa contohnya adalah `kcov.id/handbook` untuk mengakses *handbook* ini, `kcov.id/product-ssot` untuk dokumen Quip dari produk, hingga `kcov.id/superspreader` untuk konten *post* tentang *superspreader* di *website* `kawalcovid19.id`.

Terdapat dua bentuk yang dapat digunakan untuk pranala yang telah disingkat yaitu:

- `kcov.id/{path}` misalnya `kcov.id/handbook`. Daftar pranala asli dan bentuk singkatnya dapat diakses di `kcov.id/shorten` yang akan mengarahkan anda ke file `_redirects` di repositori GitLab `kcov.id`.
- ataupun menggunakan *subpath* dengan bentuk `kcov.id/{subpath}[/,~,!,&]{path}` seperti `kcov.id/handbook~1` untuk *issue* GitLab nomor 1 pada repository *handbook* atau `kcov.id/gitlab&3` yang akan mengarahkan anda ke *Epics* nomor 3 di grup GitLab KawalCOVID19. Bentuk ini lebih sering digunakan untuk mempermudah koordinasi dalam tim dan pembangunan dalam menggunakan GitLab. Daftar kombinasi *subpath* beserta simbolnya dan hasil *redirect*-nya dapat dilihat di berkas [*netlify.toml*](https://gitlab.com/kawalcovid19/website/kcov.id/-/blob/master/netlify.toml) ini.

Terdapat beberapa simbol yang dapat digunakan pada bentuk kedua yang menggunakan *subpath* yakni:

- Simbol `&` dengan bentuk lengkap `kcov.id/{subpath}&{n}` yang mendefinisikan *Epics* nomor `n` yang ada dalam grup ataupun *subgroup* pada GitLab KawalCOVID19. Contohnya adalah `kcov.id/gitlab&3` yang akan mengarahkan anda ke *Epics* nomor 3 di grup KawalCOVID19 ataupun `kcov.id/kawal-diri&1` untuk *Epics* nomor 1 dari *subgroup*/tim kawal-diri. `/gitlab&` digunakan untuk *Epics* keseluruhan grup KawalCOVID19 dan `/{nama-tim}&` digunakan untuk *Epics* tim atau *subgroup*.
- Simbol `~` dengan bentuk lengkap `kcov.id/{subpath}~{n}` yang mendefinisikan *Issue* nomor `n` yang ada dalam suatu repositori di grup GitLab KawalCOVID19. Contohnya adalah `kcov.id/handbook~4` yang akan mengarahkan anda ke *Issue* nomor 4 pada repositori `handbook`.
- Simbol `!` dengan bentuk lengkap `kcov.id/{subpath}!{n}` yang mendefinisikan *Merge Request* nomor `n` yang ada dalam suatu repository di grup GitLab KawalCOVID19. Contohnya adalah `kcov.id/kawalcovid19.id!2` yang akan mengarahkan anda ke *Merge Request* nomor 2 pada repositori situs `kawalcovid19.id`.
- Simbol `/` bekerja sebagai *subpath* seperti biasanya.

## Mempersingkat URL Menggunakan kcov.id

Bagian ini menjelaskan bagaimana menggunakan *link shortener* `kcov.id` untuk menyingkat pranala menjadi `kcov.id/{path}`.
Seperti yang telah dijelaskan di atas bahwa semua daftar `path` berikut dengan pranala lengkap disimpan pada berkas `_redirects` di repositori `kcov.id` pada GitLab KawalCOVID19 di *subgroup* `website` sehingga cara untuk menyingkat pranalanya adalah dengan mengubah berkas tersebut seperti berikut:

- *Raise* *Merge Request* dengan cara yang biasanya dipakai untuk berkontribusi ke suatu repositori baru (*clone* repositori [`kcov.id`](https://gitlab.com/kawalcovid19/website/kcov.id), buat *branch* baru, tambahkan kode yang berisi *path* dan pranala lengkap pada berkas [`_redirects`](https://gitlab.com/kawalcovid19/website/kcov.id/-/blob/master/_redirects) di mesin lokal, *commit* kode, dan *push* ke repository `kcov.id`).
- Nama *branch* sebaiknya disertai dengan *path* pranala singkat yang ditambahkan.
- Kode yang ditambahkan berupa 1 baris kode dengan `/path` (dimulai dengan `/`) dan pranala lengkap sebagai hasil *redirect*. Tabulasi disesuaikan dengan entri yang sudah ada.
- *Merge Request* akan di-*review* dan di-*merge* oleh *maintainer*.

## Menambahkan Aturan *redirect* Menggunakan Netlify

Bagian ini menjelaskan bagaimana menggunakan *link shortener* `kcov.id` yang memiliki aturan tambahan baru dengan adanya *subpath* dengan berbagai simbol yang memungkinkan seperti pada bentuk kedua yang telah dijelaskan di atas. Seperti yang telah dijelaskan di atas bahwa semua daftar aturan kombinasi simbol dan `subpath` berikut dengan pranala hasil *redirect* disimpan pada berkas `netlify.toml` di repositori `kcov.id` pada GitLab KawalCOVID19 di *subgroup* `website` sehingga cara untuk menyingkat pranalanya adalah dengan mengubah berkas tersebut seperti berikut:

- *Raise* *Merge Request* dengan cara yang biasanya dipakai untuk berkontribusi ke suatu repositori baru (*clone* repositori [`kcov.id`](https://gitlab.com/kawalcovid19/website/kcov.id), buat *branch* baru, tambahkan kode yang berisi kombinasi *subpath* dan simbol, serta pranala hasil *redirect* pada berkas [`netlify.toml`](https://gitlab.com/kawalcovid19/website/kcov.id/-/blob/master/netlify.toml) di mesin lokal, *commit* kode, dan *push* ke repository `kcov.id`).
- Nama *branch* sebaiknya disertai dengan aturan *subpath* pranala singkat yang ditambahkan.
- Kode yang ditambahkan berupa 3 baris kode yang dimulai dengan `[[redirects]]`, `from = "/{subpath}~*"` yang mendefinisikan pranala hasil penyingkatan dengan aturan simbolnya, dan `to = "pranala-lengkap/:splat"` yang digunakan sebagai pranala tujuan hasil proses *redirect*. Contohnya adalah sebagai berikut:

```toml
[[redirects]]
  from = "/kawal-diri&*"
  to = "https://gitlab.com/groups/kawalcovid19/kawal-diri/-/epics/:splat"
[[redirects]]
  from = "/kawal-diri*"
  to = "https://gitlab.com/groups/kawalcovid19/kawal-diri/:splat"
```

- `*` pada bagian `from` dan `:splat` pada bagian `to` di atas saling berkorespondensi dan digunakan untuk menandakan semua kemungkinan `path` yang ada.
- Setiap berbagai aturan dari `subpath` yang sama dikumpulkan bersama-sama dan dipisahkan dengan 1 `new line` terhadap `subpath` baru.

Selain itu kamu juga dapat mengontak *@admin-tech* di *workspace* Slack untuk menyingkat URLmu jika kamu mengalami kesulitan.
